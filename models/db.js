var MongoClient = require('mongodb').MongoClient;
var Server = require('mongodb').Server;
var Db = require('mongodb').Db;
var settings = require('../settings');

module.exports = new Db(settings.db, new Server(settings.host, 27017, { auto_reconnect: true }));

module.exports.openCollection = function openCollection(name, callback) {
  this.open(function(err, db) {
    if (err) {
      return callback(err);
    }
    db.collection(name, callback);
  });
};
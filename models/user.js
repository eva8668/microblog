var mongodb = require('./db');

function User(user) {
  this.name = user.name;
  this.password = user.password;
}
module.exports = User;

User.prototype.save = function(callback) {
  // 存入 MongoDB
  var user = {
    name: this.name,
    password: this.password
  };
  
  mongodb.open(function(err, db) {
    if (err) {
      return callback(err);
    }
    
    // 讀取 users 集合
    db.collection('users', function(err, collection) {
      if (err) {
        mongodb.close();
        return callback(err);
      }
      
      // 為 name 屬性增加索引
      collection.ensureIndex('name', {unique: true});
      
      // 寫入 user 檔案
      collection.insert(user, {safe: true}, function(err, user) {
        mongodb.close();
        callback(err, user);
      });
    });
  });
};

User.get = function(username, callback) {
  mongodb.open(function(err, db) {
    if (err) {
      return callback(err);
    }
    
    // 讀取 users 集合
    db.collection('users', function(err, collection) {
      if (err) {
        mongodb.close();
        return callback(err);
      }
      
      // 尋找 name 屬性為 username 的檔案
      collection.findOne({name: username}, function(err, doc) {
        mongodb.close();
        if (doc) {
          // 封裝檔案為 User 物件
          var user = new User(doc);
          callback(err, user);
        } else {
          callback(err, null);
        }
      });
    });
  });
};
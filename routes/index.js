var User = require('../models/user');
var Post = require('../models/post');
var crypto = require('crypto');
var moment = require('moment');

exports.route = function(app) {
  app.get('/', function(req, res) {
    Post.get(null, function(err, posts) {
      if (err) {
        posts = [];
      }
      
      res.render('index', {
        title: '首頁',
        posts: formatPost(posts)
      });
    });
  });
  
  app.get('/reg', checkNotLogin);
  app.get('/reg', function(req, res) {
    res.render('reg', {
      title: '使用者註冊'
    });
  });
  
  app.post('/reg', checkNotLogin);
  app.post('/reg', function(req, res) {
    // 檢驗使用者兩次輸入的密碼是否一致
    if (req.body['password-repeat'] != req.body['password']) {
      req.flash('error', '兩次輸入的密碼不一致！');
      return res.redirect('/reg');
    }
    
    // 產生密碼的雜湊值
    var md5 = crypto.createHash('md5');
    var password = md5.update(req.body.password).digest('base64');
    
    var newUser = new User({
      name: req.body.username,
      password: password
    });
    
    // 檢查使用者是否存在
    User.get(newUser.name, function(err, user) {
      if (user) {
        err = 'User already exists.';
      }
      
      if (err) {
        req.flash('error', err);
        return res.redirect('/reg');
      }
      
      // 如果不存在則新增使用者
      newUser.save(function(err) {
        if (err) {
          req.flash('error', err);
          return res.redirect('/reg');
        }
        
        req.session.user = newUser;
        req.flash('success', '註冊成功');
        res.redirect('/');
      });
    });
  });
  
  app.get('/login', checkNotLogin);
  app.get('/login', function(req, res) {
    res.render('login', {
      title: '登入'
    });
  });
  
  app.post('/login', checkNotLogin);
  app.post('/login', function(req, res) {
    // 產生密碼的雜湊值
    var md5 = crypto.createHash('md5');
    var password = md5.update(req.body.password).digest('base64');
    
    User.get(req.body.username, function(err, user) {
      if (!user) {
        req.flash('error', '使用者不存在');
        return res.redirect('/login');
      }
      
      if (user.password != password) {
        req.flash('error', '使用者密碼錯誤');
        return res.redirect('/login');
      }
      
      req.session.user = user;
      req.flash('success', '登入成功');
      return res.redirect('/');
    });
  });
  
  app.get('/logout', checkLogin);
  app.get('/logout', function(req, res) {
    req.session.user = null;
    req.flash('success', '登出成功');
    return res.redirect('/');
  });
  
  app.post('/post', checkLogin);
  app.post('/post', function(req, res) {
    var currentUser = req.session.user;
    var post = new Post(currentUser.name, req.body.post);
    post.save(function(err) {
      if (err) {
        req.flash('error', err);
        return res.redirect('/');
      }
      
      req.flash('success', '發表成功');
      return res.redirect('/u/' + currentUser.name);
    });
  });
  
  app.get('/u/:user', function(req, res) {
    User.get(req.params.user, function(err, user) {
      if (!user) {
        req.flash('error', '使用者不存在');
        return res.redirect('/');
      }
      
      Post.get(user.name, function(err, posts) {
        if (err) {
          req.flash('error', err);
          return res.redirect('/');
        }
        
        res.render('user', {
          title: user.name,
          posts: formatPost(posts)
        });
      });
    });
  });
  
  function checkLogin(req, res, next) {
    if (!req.session.user) {
      req.flash('error', '您尚未登入');
      return res.redirect('/login');
    }
    next();
  }
  
  function checkNotLogin(req, res, next) {
    if (req.session.user) {
      req.flash('error', '您已經登入了');
      return res.redirect('/');
    }
    next();
  }
  
  function formatPost(posts) {
    posts.forEach(function(post, index) {
      post.timeString = moment(post.time).fromNow();
    });
    return posts;
  }
  
};